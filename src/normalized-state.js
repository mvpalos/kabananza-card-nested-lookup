import { schema, normalize } from 'normalizr';
import defaultState from './default-state';

const user = new schema.Entity('users');
const card = new schema.Entity('cards', {assignedTO: user});
const list = new schema.Entity('list', {
    cards: [card]
})
const normalizedList = normalize(defaultState.lists, [list]);
const normalizedUsers = normalize(defaultState.users, [user]);

export const lists = {
    entities: normalizedList.entities.list,
    ids: normalizedList.result,
}

export const users = {
    entities: normalizedUsers.entities.users,
    ids: normalizedUsers.result
}

export const cards = {
    entities: normalizedList.entities.cards,
    ids: Object.keys(normalizedList.entities.cards)
}

export default {
    users,
    lists,
    cards
}